# maven-dependency-graph

Visualization of Maven-based dependency relationships 

# Build

    $ docker build -t maven-dependency-graph .

or

    $ make image

# Run

    $ docker run --rm -it -p 8080:80 maven-dependency-graph

or

    $ make run