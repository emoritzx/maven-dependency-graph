import logging
from urllib.request import Request, urlopen
from maven.api import SearchAPI

logger = logging.getLogger('maven.central')

class MavenCentral(SearchAPI):

    def __init__(self):
        self.url = "https://search.maven.org"

    def _get_pom_data(self, coords):
        pom_url = self._get_pom_url(coords)
        logger.debug("POM URL: %s" % pom_url)
        request = Request(pom_url)
        with urlopen(request) as response:
            return response.read()

    def _get_pom_url(self, coords):
        return "%s/remotecontent?filepath=%s/%s/%s/%s-%s.pom" % (self.url, coords.group.replace('.', '/'), coords.name, coords.version, coords.name, coords.version)
