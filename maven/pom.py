import logging
from xml.dom import minidom

logger = logging.getLogger('maven.pom')

def parse(pom):
    saved_dependencies = []
    dom = minidom.parseString(pom)
    properties = get_properties(dom)
    dependencies = dom.getElementsByTagName("dependency")
    for dependency in dependencies:
        scope_node = dependency.getElementsByTagName("scope")
        if len(scope_node) == 0 or scope_node[0].firstChild.wholeText == "compile":
            try:
                groupId = check_property(properties, dependency.getElementsByTagName('groupId')[0].firstChild.wholeText)
                artifactId = check_property(properties, dependency.getElementsByTagName('artifactId')[0].firstChild.wholeText)
                version = check_property(properties, dependency.getElementsByTagName('version')[0].firstChild.wholeText)
                saved_dependencies.append(Coordinates(groupId, artifactId, version))
            except Exception as e:
                logger.warning("Handle edge case %s" % str(e))
    return saved_dependencies

def get_properties(dom):
    properties_node_list = dom.getElementsByTagName("properties")
    if len(properties_node_list) == 0:
        return { }
    properties_node = properties_node_list[0]
    properties = { }
    for node in properties_node.childNodes:
        child = node.firstChild
        if child is not None:
            properties[node.localName] = child.wholeText
    return properties

def check_property(properties, name):
    if name.startswith('$'):
        property_name = extract_property(name)
        return properties[property_name]
    else:
        return name

def extract_property(name):
    return name[2:len(name)-1]

class Coordinates:

    @staticmethod
    def extract(coordinates):
        parts = coordinates.split(':')
        return parts[0:3]

    def __init__(self,  group, name, version):
        self.group = group
        self.name = name
        self.version = version

    @classmethod
    def from_string(cls, coordinates):
        group, name, version = Coordinates.extract(coordinates)
        return cls(group, name, version)

    def __str__(self):
        return "%s:%s:%s" % (self.group, self.name, self.version)
