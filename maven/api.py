import logging
from maven import pom
from maven.pom import Coordinates
from urllib.error import HTTPError

logger = logging.getLogger('maven.api')

class SearchAPI:

    def get_dependencies(self, coords, recursive=False):
        stack = [ Coordinates.from_string(coords) ]
        saved_dependencies = { }
        while len(stack) > 0:
            current_dependency = stack.pop()
            logger.debug("parsing %s" % str(current_dependency))
            try:
                xml = self._get_pom_data(current_dependency)
                adjacencies = pom.parse(xml)
            except HTTPError as e:
                logger.warning("Fix error %s" % str(e))
                adjacencies = [ ]
            saved_dependencies[str(current_dependency)] = Dependency(current_dependency, adjacencies)
            if recursive:
                for adjacency in adjacencies:
                    if str(adjacency) not in saved_dependencies:
                        stack.append(adjacency)
        return saved_dependencies.values()

    def _get_pom_data(self, coords):
        raise Exception("You must load an implementation plugin (e.g. MavenCentral)")

class Dependency:

    def __init__(self, coords, adjacencies):
        self.coords = coords
        self.adjacencies = adjacencies
