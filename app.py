#!/usr/bin/env python3

# import external adjacencies
from flask import Flask, render_template, request, jsonify
import logging

# import app adjacencies
import jit
from maven.central import MavenCentral

# creates a Flask application, named app
app = Flask('maven-dependency-graph')
logging.basicConfig(level=logging.DEBUG)
api = MavenCentral()

@app.route("/")
def index():
    return render_template("form.html")

@app.route("/graph", methods=["GET"])
def graph():
    app.logger.info(request.form)
    coordinates = request.args.get("coordinates")
    #downstream_depth = int(request.args.get("downstream-depth"))
    #upstream_depth = int(request.args.get("upstream-depth"))
    dependencies = api.get_dependencies(coordinates, True)
    graph = jit.convert(dependencies)
    return jsonify(graph)

def create_arg_parser():
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("--port", required=True)
    return parser

# run the application
if __name__ == "__main__":
    # parse arguments
    parser = create_arg_parser()
    args = parser.parse_args()
    # run app
    app.run(port=args.port, host='0.0.0.0')
