# Makefile

# variables

PYTHON := python3
STATIC_DIR := static
JIT := https://philogb.github.io/jit/downloads/Jit-2.0.1.zip
JIT_DIR := $(STATIC_DIR)/js/jit
HOST_PORT := 8080

# rules

.PHONY: all clean jit requirements run image

all: jit

requirements: requirements.txt
	$(PYTHON) -m pip install -r $<

jit: TMPFILE := $(shell mktemp)
jit:
	wget $(JIT) -O $(TMPFILE)
	unzip -d $(JIT_DIR) $(TMPFILE)
	rm $(TMPFILE)

clean:
	rm -f $(JIT_DIR)

image:
	docker build -t maven-dependency-graph .

run:
	docker run --rm -it -p $(HOST_PORT):80 maven-dependency-graph