
def convert(dependencies):
    return [ Node(str(dependency.coords), [ str(adj) for adj in dependency.adjacencies ]).__dict__ for dependency in dependencies ]

class Node:

    def __init__(self, name, adjacencies=[], shape="circle", color="grey"):
        self.name = name
        self.id = name
        self.adjacencies = adjacencies
        self.shape = shape
        self.color = color