FROM alpine:latest AS build
RUN apk add make wget unzip
WORKDIR /build
COPY . .
RUN make

FROM python:3 AS app
WORKDIR /usr/src/app
COPY --from=build /build .
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 80
ENTRYPOINT python ./app.py --port 80